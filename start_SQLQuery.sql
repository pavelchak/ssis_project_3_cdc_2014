USE master
go

-- 1 Create DB_Source
DROP DATABASE IF EXISTS [PavelchakSourceCDC]
go

CREATE DATABASE [PavelchakSourceCDC]
GO
USE [PavelchakSourceCDC]
GO

-- 2 Create Table_Source
DROP TABLE IF EXISTS [Person]
go
SELECT [BusinessEntityID]
      ,[PersonType]
      ,[NameStyle]
      ,[Title]
      ,[FirstName]
      ,[MiddleName]
      ,[LastName]
      ,[Suffix]
      ,[EmailPromotion]
      ,[rowguid]
      ,[ModifiedDate]
INTO [Person]
FROM [AdventureWorks2014].[Person].[Person]
WHERE BusinessEntityID <= 100
go

-- add a primary key to the [Person] table so we can enable support for net changes
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Person]') AND name = N'PK_Person_CDC')
ALTER TABLE [dbo].[Person] ADD CONSTRAINT [PK_Person_CDC] PRIMARY KEY CLUSTERED
(
    [BusinessEntityID] ASC
)
go

SELECT * FROM Person
go

-------------------------------------------------------------------------
USE master
go
-- 1 Create DB_Destination
DROP DATABASE IF EXISTS [PavelchakDestinationCDC]
go

CREATE DATABASE [PavelchakDestinationCDC]
GO
USE [PavelchakDestinationCDC]
GO
-- 3 Create Table_Destination
DROP TABLE IF EXISTS [Person]
go
SELECT TOP 0
	   [BusinessEntityID]
      ,[PersonType]
      ,[NameStyle]
      ,[Title]
      ,[FirstName]
      ,[MiddleName]
      ,[LastName]
      ,[Suffix]
      ,[EmailPromotion]
      ,[rowguid]
      ,[ModifiedDate]
INTO [Person]
FROM [AdventureWorks2014].[Person].[Person]
go

SELECT * FROM Person
go 

-------------------------------------------------------------------------

USE [PavelchakSourceCDC]
go

-- enable CDC for DB
EXEC sys.sp_cdc_enable_db
go

-- enable CDC for table
EXEC sys.sp_cdc_enable_table
@source_schema = N'dbo',
@source_name = N'Person',
@role_name = N'cdc_admin',
@supports_net_changes = 1
go




